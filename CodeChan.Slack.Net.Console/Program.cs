﻿using System.Diagnostics;
using CodeChan.Slack.Net.Api;

namespace CodeChan.Slack.Net.Console
{
    using Console = System.Console;
    class Program
    {
        static void Main()
        {
            using (var slack = new RtmSlackClient(Const.Token))
            {
                slack.OnSocketMessageReceived += (o, e) => Console.WriteLine(e);
                slack.OnRtmStart += (_, r) =>
                {
                    foreach (var channel in r.Data.Channels)
                        Trace.WriteLine(string.Format("{0} - {1}", channel.Id, channel.Name));
                };
                slack.Connect().Wait();
                
                for (var str = Console.ReadLine(); !string.IsNullOrEmpty(str); str = Console.ReadLine())
                {
                    slack.SendMessage(Const.General, str);
                }
            }
        }
    }
}
