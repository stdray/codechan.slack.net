﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CodeChan.Slack.Net.Api.Model;
using Newtonsoft.Json.Linq;
using WebSocket4Net;

namespace CodeChan.Slack.Net.Api
{
    public class RtmSlackClient : SlackClientBase
    {
        private WebSocket _socket;
        private int _messageId = 0;

        public RtmSlackClient(string token)
        {
            Token = token;
        }

        #region Socket events

        public event EventHandler<Exception> OnSocketError = (o, e) => { };
        public event EventHandler OnSocketClosed = (o, e) => { };
        public event EventHandler<byte[]> OnSocketDataReceived = (o, e) => { };
        public event EventHandler<string> OnSocketMessageReceived = (o, e) => { };
        public event EventHandler OnSocketOpened = (o, e) => { };

        #endregion

        #region Events

        public event EventHandler<RpcResponse<RtmStartResponse>> OnRtmStart = (o, e) => { };

        #endregion

        #region Methods

        public override async Task Connect(CancellationToken cancellation)
        {
            await base.Connect(cancellation);
            var start = await RtmStart(cancellation);
            _socket = RunWebSocket(start.Data.Url);
            OnRtmStart.Invoke(this, start);
        }

        public void SendPing(IEnumerable<KeyValuePair<string, string>> extraArgs = null)
        {
            Send(MessageType.Ping, null, null, null);
        }

        public void SendTyping(string channel)
        {
            Send(MessageType.Typing, channel, null, null);
        }

        public void SendMessage(string channel, string text)
        {
            Send(MessageType.Message, channel, text, null);
        }

        protected void Send(string type, string channel, string text, IEnumerable<KeyValuePair<string, string>> extraArgs)
        {
            var json = new JObject
            {
                {"id", _messageId++}, 
                {"type", type}, 
            };
            if (channel != null)
                json.Add("channel", channel);
            if (text != null)
                json.Add("text", text);
            if (extraArgs != null)
                foreach (var arg in extraArgs)
                    json.Add(arg.Key, arg.Value);
            var str = json.ToString(JsonSerializerSettings.Formatting, JsonConverters);
            _socket.Send(str);
        }

        #endregion

        #region Helpers

        private WebSocket RunWebSocket(string url)
        {
            var socket = new WebSocket(url);
            socket.Closed += (_, e) => OnSocketClosed(this, e);
            socket.DataReceived += (_, e) => OnSocketDataReceived(this, e.Data);
            socket.Error += (_, e) => OnSocketError(this, e.Exception);
            socket.MessageReceived += (_, e) => OnSocketMessageReceived(this, e.Message);
            socket.Opened += (_, e) => OnSocketOpened(this, e);
            socket.Open();
            return socket;
        }

        protected override Task<string> GetToken(CancellationToken cancellation)
        {
            return Task.FromResult(Token);
        }

        #endregion

        #region Dispose

        protected override void Dispose(bool disposing)
        {
            if (_socket != null)
                _socket.Dispose();
            base.Dispose(disposing);
        }

        #endregion

    }
}