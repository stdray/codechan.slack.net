﻿using Newtonsoft.Json;

namespace CodeChan.Slack.Net.Api.Model
{
    public class ImLatest
    {

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("user")]
        public string User { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("ts")]
        public string Ts { get; set; }
    }
}