﻿using Newtonsoft.Json;

namespace CodeChan.Slack.Net.Api.Model
{
    public class PinnedItem
    {

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("channel")]
        public string Channel { get; set; }

        [JsonProperty("message")]
        public Message Message { get; set; }
    }
}