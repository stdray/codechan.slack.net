﻿using Newtonsoft.Json;

namespace CodeChan.Slack.Net.Api.Model
{
    public class Message
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("channel")]
        public string Channel { get; set; }

        [JsonProperty("user")]
        public string User { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("ts")]
        public string Ts { get; set; }

        [JsonProperty("edited")]
        public Edited Edited { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("subtype")]
        public string Subtype { get; set; }

        [JsonProperty("permalink")]
        public string Permalink { get; set; }

        [JsonProperty("pinned_to")]
        public string[] PinnedTo { get; set; }

        [JsonProperty("reply_to")]
        public int? ReplyTo { get; set; }

        [JsonProperty("ok")]
        public bool Ok { get; set; }
    }
}