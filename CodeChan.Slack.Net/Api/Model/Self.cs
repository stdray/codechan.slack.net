using Newtonsoft.Json;

namespace CodeChan.Slack.Net.Api.Model
{
    public class Self
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("prefs")]
        public SelfPrefs SelfPrefs { get; set; }

        [JsonProperty("created")]
        public int Created { get; set; }

        [JsonProperty("manual_presence")]
        public string ManualPresence { get; set; }
    }
}