﻿using Newtonsoft.Json;

namespace CodeChan.Slack.Net.Api.Model
{
    public class RtmStartResponse
    {

        [JsonProperty("ok")]
        public bool Ok { get; set; }

        [JsonProperty("self")]
        public Self Self { get; set; }

        [JsonProperty("team")]
        public Team Team { get; set; }

        [JsonProperty("latest_event_ts")]
        public string LatestEventTs { get; set; }

        [JsonProperty("channels")]
        public Channel[] Channels { get; set; }

        [JsonProperty("groups")]
        public object[] Groups { get; set; }

        [JsonProperty("ims")]
        public Im[] Ims { get; set; }

        [JsonProperty("users")]
        public User[] Users { get; set; }

        [JsonProperty("bots")]
        public Bot[] Bots { get; set; }

        [JsonProperty("cache_version")]
        public string CacheVersion { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }

}
