using Newtonsoft.Json;

namespace CodeChan.Slack.Net.Api.Model
{
    public class InitialComment
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("created")]
        public int Created { get; set; }

        [JsonProperty("timestamp")]
        public int Timestamp { get; set; }

        [JsonProperty("user")]
        public string User { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }
    }
}