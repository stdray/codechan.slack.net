using Newtonsoft.Json;

namespace CodeChan.Slack.Net.Api.Model
{
    public class TaemPrefs
    {

        [JsonProperty("default_channels")]
        public string[] DefaultChannels { get; set; }

        [JsonProperty("gateway_allow_xmpp_ssl")]
        public int GatewayAllowXmppSsl { get; set; }

        [JsonProperty("gateway_allow_irc_ssl")]
        public int GatewayAllowIrcSsl { get; set; }

        [JsonProperty("gateway_allow_irc_plain")]
        public int GatewayAllowIrcPlain { get; set; }

        [JsonProperty("msg_edit_window_mins")]
        public int MsgEditWindowMins { get; set; }

        [JsonProperty("allow_message_deletion")]
        public bool AllowMessageDeletion { get; set; }

        [JsonProperty("disable_builtin_loading")]
        public bool DisableBuiltinLoading { get; set; }

        [JsonProperty("hide_referers")]
        public bool HideReferers { get; set; }

        [JsonProperty("display_real_names")]
        public bool DisplayRealNames { get; set; }

        [JsonProperty("who_can_at_everyone")]
        public string WhoCanAtEveryone { get; set; }

        [JsonProperty("who_can_at_channel")]
        public string WhoCanAtChannel { get; set; }

        [JsonProperty("warn_before_at_channel")]
        public string WarnBeforeAtChannel { get; set; }

        [JsonProperty("who_can_create_channels")]
        public string WhoCanCreateChannels { get; set; }

        [JsonProperty("who_can_archive_channels")]
        public string WhoCanArchiveChannels { get; set; }

        [JsonProperty("who_can_create_groups")]
        public string WhoCanCreateGroups { get; set; }

        [JsonProperty("who_can_post_general")]
        public string WhoCanPostGeneral { get; set; }

        [JsonProperty("who_can_kick_channels")]
        public string WhoCanKickChannels { get; set; }

        [JsonProperty("who_can_kick_groups")]
        public string WhoCanKickGroups { get; set; }

        [JsonProperty("retention_type")]
        public int RetentionType { get; set; }

        [JsonProperty("retention_duration")]
        public int RetentionDuration { get; set; }

        [JsonProperty("group_retention_type")]
        public int GroupRetentionType { get; set; }

        [JsonProperty("group_retention_duration")]
        public int GroupRetentionDuration { get; set; }

        [JsonProperty("dm_retention_type")]
        public int DmRetentionType { get; set; }

        [JsonProperty("dm_retention_duration")]
        public int DmRetentionDuration { get; set; }

        [JsonProperty("require_at_for_mention")]
        public int RequireAtForMention { get; set; }

        [JsonProperty("compliance_export_start")]
        public int ComplianceExportStart { get; set; }
    }
}