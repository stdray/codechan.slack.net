﻿using Newtonsoft.Json;

namespace CodeChan.Slack.Net.Api.Model
{
    public class Im
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("is_im")]
        public bool IsIm { get; set; }

        [JsonProperty("user")]
        public string User { get; set; }

        [JsonProperty("created")]
        public int Created { get; set; }

        [JsonProperty("is_user_deleted")]
        public bool IsUserDeleted { get; set; }

        [JsonProperty("last_read")]
        public string LastRead { get; set; }

        [JsonProperty("latest")]
        public ImLatest Latest { get; set; }

        [JsonProperty("unread_count")]
        public int UnreadCount { get; set; }

        [JsonProperty("unread_count_display")]
        public int UnreadCountDisplay { get; set; }

        [JsonProperty("is_open")]
        public bool IsOpen { get; set; }
    }
}