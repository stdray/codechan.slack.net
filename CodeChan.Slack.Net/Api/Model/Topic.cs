﻿using Newtonsoft.Json;

// ReSharper disable once CheckNamespace
namespace CodeChan.Slack.Net.Api.Model
{
    public class Topic
    {

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("creator")]
        public string Creator { get; set; }

        [JsonProperty("last_set")]
        public int LastSet { get; set; }
    }
}
