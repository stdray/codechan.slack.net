﻿using Newtonsoft.Json;

namespace CodeChan.Slack.Net.Api.Model
{
    public class Bot
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("deleted")]
        public bool Deleted { get; set; }

        [JsonProperty("icons")]
        public Icon Icons { get; set; }
    }
}