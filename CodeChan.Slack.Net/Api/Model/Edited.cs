using Newtonsoft.Json;

namespace CodeChan.Slack.Net.Api.Model
{
    public class Edited
    {

        [JsonProperty("user")]
        public string User { get; set; }

        [JsonProperty("ts")]
        public string Ts { get; set; }
    }
}