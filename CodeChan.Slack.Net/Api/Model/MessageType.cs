﻿namespace CodeChan.Slack.Net.Api.Model
{
    public static class MessageType
    {
        public const string Hello = "hello";
        public const string Message = "message";
        public const string Ping = "ping";
        public const string Pong = "pong";
        public const string PresenceChange = "presence_change";
        public const string Typing = "typing";
    }
}