﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CodeChan.Slack.Net.Api.Model
{
    public class Group
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("is_group")]
        public string IsGroup { get; set; }

        [JsonProperty("created")]
        public int Created { get; set; }

        [JsonProperty("creator")]
        public string Creator { get; set; }

        [JsonProperty("is_archived")]
        public bool IsArchived { get; set; }

        [JsonProperty("members")]
        public string[] Members { get; set; }

        [JsonProperty("topic")]
        public Topic Topic { get; set; }

        [JsonProperty("purpose")]
        public Purpose Purpose { get; set; }

        [JsonProperty("last_read")]
        public string LastRead { get; set; }

        [JsonProperty("unread_count")]
        public int UnreadCount { get; set; }

        [JsonProperty("unread_count_display")]
        public int UnreadCountDisplay { get; set; }

        [JsonProperty("latest")]
        public IList<Message> Latest { get; set; }
    }
}