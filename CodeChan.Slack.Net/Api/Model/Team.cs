﻿using Newtonsoft.Json;

namespace CodeChan.Slack.Net.Api.Model
{
    public class Team
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("email_domain")]
        public string EmailDomain { get; set; }

        [JsonProperty("domain")]
        public string Domain { get; set; }

        [JsonProperty("msg_edit_window_mins")]
        public int MsgEditWindowMins { get; set; }

        [JsonProperty("prefs")]
        public TaemPrefs Prefs { get; set; }

        [JsonProperty("icon")]
        public Icon Icon { get; set; }

        [JsonProperty("over_storage_limit")]
        public bool OverStorageLimit { get; set; }

        [JsonProperty("plan")]
        public string Plan { get; set; }
    }
}