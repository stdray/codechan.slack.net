using Newtonsoft.Json;

namespace CodeChan.Slack.Net.Api.Model
{
    public class File
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("created")]
        public int Created { get; set; }

        [JsonProperty("timestamp")]
        public int Timestamp { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("mimetype")]
        public string Mimetype { get; set; }

        [JsonProperty("filetype")]
        public string Filetype { get; set; }

        [JsonProperty("pretty_type")]
        public string PrettyType { get; set; }

        [JsonProperty("user")]
        public string User { get; set; }

        [JsonProperty("mode")]
        public string Mode { get; set; }

        [JsonProperty("editable")]
        public bool Editable { get; set; }

        [JsonProperty("is_external")]
        public bool IsExternal { get; set; }

        [JsonProperty("external_type")]
        public string ExternalType { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("url_download")]
        public string UrlDownload { get; set; }

        [JsonProperty("url_private")]
        public string UrlPrivate { get; set; }

        [JsonProperty("url_private_download")]
        public string UrlPrivateDownload { get; set; }

        [JsonProperty("thumb_64")]
        public string Thumb64 { get; set; }

        [JsonProperty("thumb_80")]
        public string Thumb80 { get; set; }

        [JsonProperty("thumb_360")]
        public string Thumb360 { get; set; }

        [JsonProperty("thumb_360_gif")]
        public string Thumb360Gif { get; set; }

        [JsonProperty("thumb_360_w")]
        public string Thumb360W { get; set; }

        [JsonProperty("thumb_360_h")]
        public string Thumb360H { get; set; }

        [JsonProperty("permalink")]
        public string Permalink { get; set; }

        [JsonProperty("edit_link")]
        public string EditLink { get; set; }

        [JsonProperty("preview")]
        public string Preview { get; set; }

        [JsonProperty("preview_highlight")]
        public string PreviewHighlight { get; set; }

        [JsonProperty("lines")]
        public int Lines { get; set; }

        [JsonProperty("lines_more")]
        public int LinesMore { get; set; }

        [JsonProperty("is_public")]
        public bool IsPublic { get; set; }

        [JsonProperty("public_url_shared")]
        public bool PublicUrlShared { get; set; }

        [JsonProperty("channels")]
        public string[] Channels { get; set; }

        [JsonProperty("groups")]
        public string[] Groups { get; set; }

        [JsonProperty("num_stars")]
        public int NumStars { get; set; }

        [JsonProperty("is_starred")]
        public bool IsStarred { get; set; }

        [JsonProperty("thumb_160")]
        public string Thumb160 { get; set; }

        [JsonProperty("image_exif_rotation")]
        public int ImageExifRotation { get; set; }

        [JsonProperty("permalink_public")]
        public string PermalinkPublic { get; set; }

        [JsonProperty("ims")]
        public object[] Ims { get; set; }

        [JsonProperty("comments_count")]
        public int CommentsCount { get; set; }

        [JsonProperty("initial_comment")]
        public InitialComment InitialComment { get; set; }
    }
}