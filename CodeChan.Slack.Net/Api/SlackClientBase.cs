﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CodeChan.Slack.Net.Api.Model;
using CodeChan.Slack.Net.Utils;
using Newtonsoft.Json;

namespace CodeChan.Slack.Net.Api
{
    using ArgSeq = IEnumerable<KeyValuePair<string,string>>;
    using ArgCol = IReadOnlyCollection<KeyValuePair<string, string>>;

    public abstract class SlackClientBase : IDisposable
    {
       private readonly HttpClient _http;
        protected readonly JsonSerializerSettings JsonSerializerSettings;
        private JsonConverter[] _jsonConverters;

        protected JsonConverter[] JsonConverters
        {
            get
            {
                return _jsonConverters ?? (_jsonConverters = JsonSerializerSettings.Converters.ToArray());
            }
        }

        protected string Token;

        protected SlackClientBase()
        {
            JsonSerializerSettings = new JsonSerializerSettings();
            _http = new HttpClient
            {
                BaseAddress = new Uri("https://slack.com/api/")
            };
        }

        public Task Connect()
        {
            return Connect(CancellationToken.None);
        }

        public virtual async Task Connect(CancellationToken cancellation)
        {
            Token = await GetToken(cancellation);
        }

        protected abstract Task<string> GetToken(CancellationToken cancellation);

        
        #region Events

        //todo: describe handlers for all events from https://api.slack.com/rtm

        #endregion


        #region ApiMethods
        protected async Task<RpcResponse<RtmStartResponse>> RtmStart(CancellationToken cancelation)
        {
            return await Rpc<RtmStartResponse>("rtm.start", WithToken.Build(), cancelation);
        }
        #endregion

        #region Helpers

        protected async Task<RpcResponse<TResp>> Rpc<TResp>(string method, ArgCol agrs, CancellationToken cancelation)
        {
            using (var response = await _http.PostAsync(method, new FormUrlEncodedContent(agrs), cancelation))
            {
                var json = await response.Content.ReadAsStringAsync();
                var data = JsonConvert.DeserializeObject<TResp>(json, JsonSerializerSettings);
                return new RpcResponse<TResp>(FilterArgs(agrs), json, data);
            }
        }

        private ArgSeq FilterArgs(ArgSeq agrs)
        {
            return agrs.Where(a => a.Key != "token");
        }

        protected ArgsBuilder WithToken
        {
            get { return new ArgsBuilder().Add("token", Token); }
        }

        #endregion

        #region Dispose

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            _http.Dispose();
            if (disposing)
                GC.SuppressFinalize(this);
        }

        ~SlackClientBase()
        {
            Dispose(false);
        }

        #endregion
    }
}
