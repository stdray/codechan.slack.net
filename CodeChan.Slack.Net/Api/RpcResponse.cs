﻿using System;
using System.Collections.Generic;

namespace CodeChan.Slack.Net.Api
{
    public class RpcResponse<T>
    {
        public readonly IEnumerable<KeyValuePair<string, string>> Args;
        public readonly string Json;
        public readonly T Data;
        public readonly DateTime Created;

        public RpcResponse(IEnumerable<KeyValuePair<string, string>> args, string json, T data)
        {
            Created = DateTime.Now;
            Args = args;
            Json = json;
            Data = data;
        }
    }
}
