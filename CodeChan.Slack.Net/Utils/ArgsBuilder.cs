﻿using System.Collections.Generic;
using System.Globalization;

namespace CodeChan.Slack.Net.Utils
{
    public class ArgsBuilder
    {
        private readonly List<KeyValuePair<string, string>> _items = new List<KeyValuePair<string, string>>();

        public ArgsBuilder Add(KeyValuePair<string, string> item)
        {
            _items.Add(item);
            return this;
        }

        public ArgsBuilder Add(string key, string val)
        {
            _items.Add(new KeyValuePair<string, string>(key, val));
            return this;
        }
       
        public ArgsBuilder Add(string key, int val)
        {
            Add(key, val.ToString(CultureInfo.InvariantCulture));
            return this;
        }


        public ArgsBuilder Add(string key, double val)
        {
            Add(key, val.ToString(CultureInfo.InvariantCulture));
            return this;
        }

        public ArgsBuilder Add(string key, float val)
        {
            Add(key, val.ToString(CultureInfo.InvariantCulture));
            return this;
        }

        public ArgsBuilder Add(string key, decimal val)
        {
            Add(key, val.ToString(CultureInfo.InvariantCulture));
            return this;
        }

        public IReadOnlyCollection<KeyValuePair<string, string>> Build()
        {
            return _items;
        }
    }
}